# テトリスゲーム

## 🔰 Install

はじめにJavaおよびJDKをインストールしてください

http://www.oracle.com/technetwork/jp/java/javase/downloads/index.html

## 🏗 Compile

このディレクトリで以下のコマンドを実行してください

```
$ javac Tetris.java Block.java Clr.java Delete.java Seigyo.java
```

## 🖥 Play the Game

このディレクトリで以下のコマンドを実行してください

```
$ appletviewer index.html
```

しばらく待つとウィンドウが表示されゲームが起動します
