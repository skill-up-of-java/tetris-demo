/*
 * 作成日: 2004/07/27
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */

/**
 * @author wacharo
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class Delete {
    
    static int deleteCount = 0;
    static int deleteScore = 0;
    
    public static void delete(boolean[][] Status,int[][] colorStatus,int x,int y) {
        int count = 0;
        int bonus = 0;
        
        for(int s=0;s<4;s++) {
        loop:    for(int j=0;j<y;j++) {
                    for(int i=0;i<x;i++) {
                            if(Status[i][j]) {
                                continue loop;
                            }
                            if(i==x-1) {
                                for(int n=j;n>0;n--) {
                                    for(int m=0;m<x;m++) {
                                        Status[m][n] = Status[m][n-1];
                                        colorStatus[m][n] = colorStatus[m][n-1];
                                    }
                                }
                                count++;
                            }
                    }
                    
                }
        }
        if(count==4) {
            bonus=6;
        }
        deleteScore += count+bonus;
        deleteCount += count;
    }

}
		
