/*
 * 作成日: 2004/07/18
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */

import java.awt.*;

/**
 * @author wacharo
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class Block {
    public static void polygon(int xx,int yy,int block,Graphics offG,int pattern,int ran) {
        switch(ran) {
            case 0:
                if(pattern==0) {                                    /* _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|*/
                    offG.fillRect(xx,yy+block,block,block);         /*|_|*/
                    offG.fillRect(xx,yy,block,block);               /*|@|*/
                    offG.fillRect(xx,yy-block,block,block);         /*|_|*/
                    offG.fillRect(xx,yy-block*2,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy-block*2,block,block);
                }
                        
                if(pattern==1) {                                    /* _ _ _ _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|@|_|_|*/
                    offG.fillRect(xx-block,yy,block,block);
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx+block,yy,block,block);
                    offG.fillRect(xx+block*2,yy,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx+block*2,yy,block,block);
                }
                        
                if(pattern==2) {                                    /* _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|*/
                    offG.fillRect(xx,yy-block,block,block);         /*|@|*/
                    offG.fillRect(xx,yy,block,block);               /*|_|*/
                    offG.fillRect(xx,yy+block,block,block);         /*|_|*/
                    offG.fillRect(xx,yy+block*2,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx,yy+block*2,block,block);
                }
                    
                if(pattern==3) {                                    /* _ _ _ _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_|@|_|*/
                    offG.fillRect(xx+block,yy,block,block);
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx-block,yy,block,block);
                    offG.fillRect(xx-block*2,yy,block,block);
                            
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx-block*2,yy,block,block);
                }
                break;
                
            case 1:
                offG.setColor(Clr.clr[ran]);                        /* _ _ */
                offG.fillRect(xx,yy,block,block);                   /*|_|_|*/
                offG.fillRect(xx+block,yy,block,block);             /*|@|_|*/
                offG.fillRect(xx,yy-block,block,block);
                offG.fillRect(xx+block,yy-block,block,block);
                
                offG.setColor(Clr.clr[7]);
                offG.drawRect(xx,yy,block,block);
                offG.drawRect(xx+block,yy,block,block);
                offG.drawRect(xx,yy-block,block,block);
                offG.drawRect(xx+block,yy-block,block,block);
                break;
                
            case 2:
                if(pattern==0) {                                    /*   _ */
                    offG.setColor(Clr.clr[ran]);                    /*  |_|*/
                    offG.fillRect(xx,yy-block,block,block);         /* _|@|*/
                    offG.fillRect(xx,yy,block,block);               /*|_|_|*/
                    offG.fillRect(xx,yy+block,block,block);
                    offG.fillRect(xx-block,yy+block,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx-block,yy+block,block,block);
                }
                
                if(pattern==1) {                                    /* _     */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_ _ */
                    offG.fillRect(xx+block,yy,block,block);         /*|_|@|_|*/
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx-block,yy,block,block);
                    offG.fillRect(xx-block,yy-block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx-block,yy-block,block,block);
                }
                
                if(pattern==2) {                                    /* _ _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_|*/
                    offG.fillRect(xx,yy-block,block,block);         /*|@|  */
                    offG.fillRect(xx,yy,block,block);               /*|_|  */
                    offG.fillRect(xx,yy+block,block,block);
                    offG.fillRect(xx+block,yy-block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx+block,yy-block,block,block);
                }
                
                if(pattern==3) {                                    /* _ _ _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|@|_|*/
                    offG.fillRect(xx-block,yy,block,block);         /*    |_|*/
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx+block,yy,block,block);
                    offG.fillRect(xx+block,yy+block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx+block,yy+block,block,block);
                }
                break;
                
            case 3:
                if(pattern==0) {                                    /* _   */
                    offG.setColor(Clr.clr[ran]);                    /*|_|  */
                    offG.fillRect(xx,yy-block,block,block);         /*|@|_ */
                    offG.fillRect(xx,yy,block,block);               /*|_|_|*/
                    offG.fillRect(xx,yy+block,block,block);
                    offG.fillRect(xx+block,yy+block,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx+block,yy+block,block,block);
                }
                
                if(pattern==1) {                                    /* _ _ _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|@|_|*/
                    offG.fillRect(xx+block,yy,block,block);         /*|_|    */
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx-block,yy,block,block);
                    offG.fillRect(xx-block,yy+block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx-block,yy+block,block,block);
                }
                
                if(pattern==2) {                                    /* _ _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_|*/
                    offG.fillRect(xx,yy+block,block,block);         /*  |@|*/
                    offG.fillRect(xx,yy,block,block);               /*  |_|*/
                    offG.fillRect(xx,yy-block,block,block);
                    offG.fillRect(xx-block,yy-block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx-block,yy-block,block,block);
                }
                
                if(pattern==3) {                                    /*     _*/
                    offG.setColor(Clr.clr[ran]);                    /* _ _|_|*/
                    offG.fillRect(xx-block,yy,block,block);         /*|_|@|_|*/
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx+block,yy,block,block);
                    offG.fillRect(xx+block,yy-block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx+block,yy-block,block,block);
                }
                break;
                
            case 4:
                if(pattern==0) {                                    /*   _ */
                    offG.setColor(Clr.clr[ran]);                    /* _|_|*/
                    offG.fillRect(xx+block,yy-block,block,block);   /*|@|_|*/
                    offG.fillRect(xx+block,yy,block,block);         /*|_|  */
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx,yy+block,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx+block,yy-block,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                }
                
                if(pattern==1) {                                    /* _ _   */
                    offG.setColor(Clr.clr[ran]);                    /*|_|@|_ */
                    offG.fillRect(xx-block,yy,block,block);         /*  |_|_|*/
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx,yy+block,block,block);
                    offG.fillRect(xx+block,yy+block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx+block,yy+block,block,block);
                }
                
                if(pattern==2) {                                    /*   _ */
                    offG.setColor(Clr.clr[ran]);                    /* _|_|*/
                    offG.fillRect(xx,yy-block,block,block);         /*|_|@|*/
                    offG.fillRect(xx,yy,block,block);               /*|_|  */
                    offG.fillRect(xx-block,yy,block,block);
                    offG.fillRect(xx-block,yy+block,block,block);
                            
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx-block,yy+block,block,block);
                }
                    
                if(pattern==3) {                                    /* _ _   */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_|_ */
                    offG.fillRect(xx-block,yy-block,block,block);   /*  |@|_|*/
                    offG.fillRect(xx,yy-block,block,block);
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx+block,yy,block,block);
                            
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy-block,block,block);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                }
                break;
                
            case 5:
                if(pattern==0) {                                    /* _   */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_ */
                    offG.fillRect(xx-block,yy-block,block,block);   /*|_|@|*/
                    offG.fillRect(xx-block,yy,block,block);         /*  |_|*/
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx,yy+block,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy-block,block,block);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                }
                
                if(pattern==1) {                                    /*   _ _ */
                    offG.setColor(Clr.clr[ran]);                    /* _|_|_|*/
                    offG.fillRect(xx+block,yy-block,block,block);   /*|_|@|  */
                    offG.fillRect(xx,yy-block,block,block);
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx-block,yy,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx+block,yy-block,block,block);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx-block,yy,block,block);
                }
                
                if(pattern==2) {                                    /* _   */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_ */
                    offG.fillRect(xx,yy-block,block,block);         /*|@|_|*/
                    offG.fillRect(xx,yy,block,block);               /*  |_|*/
                    offG.fillRect(xx+block,yy,block,block);
                    offG.fillRect(xx+block,yy+block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx+block,yy+block,block,block);
                }
                
                if(pattern==3) {                                    /*   _ _ */
                    offG.setColor(Clr.clr[ran]);                    /* _|@|_|*/
                    offG.fillRect(xx+block,yy,block,block);         /*|_|_|  */
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx,yy+block,block,block);
                    offG.fillRect(xx-block,yy+block,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx-block,yy+block,block,block);
                }
                break;
            
            case 6:
                if(pattern==0) {                                    /*   _   */
                    offG.setColor(Clr.clr[ran]);                    /* _|_|_ */
                    offG.fillRect(xx-block,yy,block,block);         /*|_|@|_|*/
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx+block,yy,block,block);
                    offG.fillRect(xx,yy-block,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx,yy-block,block,block);
                }
                
                if(pattern==1) {                                    /* _   */
                    offG.setColor(Clr.clr[ran]);                    /*|_|_ */
                    offG.fillRect(xx,yy-block,block,block);         /*|@|_|*/
                    offG.fillRect(xx,yy,block,block);               /*|_|  */
                    offG.fillRect(xx,yy+block,block,block);
                    offG.fillRect(xx+block,yy,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                }
                
                if(pattern==2) {                                    /* _ _ _ */
                    offG.setColor(Clr.clr[ran]);                    /*|_|@|_|*/
                    offG.fillRect(xx-block,yy,block,block);         /*  |_|  */
                    offG.fillRect(xx,yy,block,block);
                    offG.fillRect(xx+block,yy,block,block);
                    offG.fillRect(xx,yy+block,block,block);
                    
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx-block,yy,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx+block,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                }
                
                if(pattern==3) {                                    /*   _ */
                    offG.setColor(Clr.clr[ran]);                    /* _|_|*/
                    offG.fillRect(xx,yy-block,block,block);         /*|_|@|*/
                    offG.fillRect(xx,yy,block,block);               /*  |_|*/
                    offG.fillRect(xx,yy+block,block,block);
                    offG.fillRect(xx-block,yy,block,block);
                        
                    offG.setColor(Clr.clr[7]);
                    offG.drawRect(xx,yy-block,block,block);
                    offG.drawRect(xx,yy,block,block);
                    offG.drawRect(xx,yy+block,block,block);
                    offG.drawRect(xx-block,yy,block,block);
                }
                break;
        }
    }
}
		
