/*
 * 作成日: 2005/06/23
 *
 * TODO この生成されたファイルのテンプレートを変更するには次へジャンプ:
 * ウィンドウ - 設定 - Java - コード・スタイル - コード・テンプレート
 */

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author ワチャロー
 *
 * TODO この生成された型コメントのテンプレートを変更するには次へジャンプ:
 * ウィンドウ - 設定 - Java - コード・スタイル - コード・テンプレート
 */
public class Tetris extends Applet implements Runnable, KeyListener {

    private  int LOWSPEED=1000,HIGHSPEED=100;
    private final int startXX=50,startYY=20;
    private final int block=10;
    private final int width=100,height=200;
    private final int margin=15;
    private int nextBoxX,nextBoxY;
    private int SPEED=LOWSPEED;
    private int x,y;
    private int xx,yy=startYY;
    private int pattern,turn;
    
    private int ran,next;
    
    private boolean[][] Status;
    private int[][] colorStatus;
        
    private Graphics offG;
    private Image img;
    private Thread th;
    private Label lblLevel,lblScore,Level,Score;

    public void init() {
        setSize(200,270);
        setBackground(Color.lightGray);
        setLayout(null);
        img = createImage(width+1,height+1);
        offG = img.getGraphics();
        
        x=width/block;
        y=height/block;
        
        nextBoxX = width+margin*2;
        nextBoxY = margin*4;
        
        lblLevel = new Label("Level:");
        lblLevel.setBounds(nextBoxX,nextBoxY+70,40,30);
        add(lblLevel);
        Level = new Label("1",Label.RIGHT);
        Level.setBounds(nextBoxX+45,nextBoxY+70,20,30);
        add(Level);
        
        lblScore = new Label("Score:");
        lblScore.setBounds(nextBoxX,nextBoxY+100,40,30);
        add(lblScore);
        Score = new Label("0",Label.RIGHT);
        Score.setBounds(nextBoxX+30,nextBoxY+100,35,30);
        add(Score);
        
        Status = new boolean[x][y+1];
        colorStatus = new int[x][y];
        
        for(int i=0;i<x;i++) {
            for(int j=0;j<y;j++) {
                Status[i][j] = true;
            }
        }
        
        xx=startXX;
        
        ran = (int)(Math.random()*10)%7;
        next = (int)(Math.random()*10)%7;
        
        addKeyListener(this);
        setVisible(true);
        requestFocus();
    }
    
    public void start() {
        if(th==null) {
            th = new Thread(this);
            th.start();
        }
    }
    
    public void paint(Graphics G) {
        offG.clearRect(0,0,width,height);
        offG.setColor(Color.gray);
        offG.fillRect(0,0,width,height);
        offG.setColor(Color.BLACK);
        offG.drawRect(0,0,width,height);
        
        Block.polygon(xx,yy,block,offG,pattern,ran);
        
        for(int i=0;i<x;i++) {
            for(int j=0;j<y;j++) {
                if(Status[i][j]==false) {
                    offG.setColor(Clr.clr[colorStatus[i][j]]);
                    offG.fillRect(i*block,j*block,block,block);
                    offG.setColor(Color.black);
                    offG.drawRect(i*block,j*block,block,block);
                }
            }
        }
        
        G.drawImage(img,margin,margin*2,this);
        
        G.setColor(Color.DARK_GRAY);
        G.fillRect(nextBoxX,nextBoxY,50,60);
        G.setColor(Color.BLACK);
        G.drawRect(nextBoxX,nextBoxY,50,60);
        Block.polygon(nextBoxX+block*2,nextBoxY+block*3,block,G,0,next);
    }
    
    public void update(Graphics g) {
        paint(g);
    }
    
    /* (非 Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        // TODO 自動生成されたメソッド・スタブ
        while(th == Thread.currentThread()) {
            try {
                Thread.sleep(SPEED);
            } catch (InterruptedException e) {
                // TODO 自動生成された catch ブロック
                e.printStackTrace();
            }
            repaint();
            if(Seigyo.downCheck(xx,yy,block,Status,colorStatus,pattern,ran)) {
                Delete.delete(Status,colorStatus,x,y);
                
                Score.setText(new Integer(Delete.deleteScore).toString());
                
                Level.setText(new Integer(Delete.deleteCount/5+1).toString());
                
                LOWSPEED = 1000-(Delete.deleteCount/5)*100;
                
                yy=startYY;
                xx=startXX;
                ran = next;
                next = (int)(Math.random()*10)%7;
            
            }
            yy += block;
        }
    }

    /* (非 Javadoc)
     * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
     */
    public void keyPressed(KeyEvent e) {
        // TODO 自動生成されたメソッド・スタブ
        if(e.getKeyCode()==KeyEvent.VK_LEFT) {
            if(xx>=block && Seigyo.leftCheck(xx,yy,width,block,Status,pattern,ran)) {
                xx-=block;
            }
            repaint();
            
            if(Seigyo.downCheck(xx,yy,block,Status,colorStatus,pattern,ran)) {
                yy=startYY;
                xx=startXX;
                ran = (int)(Math.random()*10)%2;
            }
        }
        
        if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if(xx<width-block && Seigyo.rightCheck(xx,yy,width,block,Status,pattern,ran)) {
                xx+=block;
            }
            repaint();
            
            if(Seigyo.downCheck(xx,yy,block,Status,colorStatus,pattern,ran)) {
                yy=startYY;
                xx=startXX;
                ran = (int)(Math.random()*10)%2;
            }
        }
        
        if(e.getKeyCode() == KeyEvent.VK_DOWN) {
            SPEED = HIGHSPEED;
        }
    }

    /* (非 Javadoc)
     * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
     */
    public void keyReleased(KeyEvent e) {
        // TODO 自動生成されたメソッド・スタブ
        if(e.getKeyCode() == KeyEvent.VK_DOWN) {
            SPEED = LOWSPEED;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_SPACE && Seigyo.rightTurnCheck(xx,yy,width,block,Status,pattern,ran)) {
            turn++;
            pattern = turn%4;
            repaint();
        }
    }

    /* (非 Javadoc)
     * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
     */
    public void keyTyped(KeyEvent e) {
        // TODO 自動生成されたメソッド・スタブ

    }
}
		

