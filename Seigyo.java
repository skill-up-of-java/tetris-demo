/*
 * 作成日: 2004/07/18
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
 
/**
 * @author wacharo
 *
 * この生成されたコメントの挿入されるテンプレートを変更するため
 * ウィンドウ > 設定 > Java > コード生成 > コードとコメント
 */
public class Seigyo {
    private static boolean bool;
    
    public static boolean downCheck(int xx,int yy,int block,boolean [][] Status,int [][] colorStatus,int pattern,int ran) {
        
        bool=false;
        
        switch(ran) {
            case 0:
                if(pattern==0) {
                    if(Status[xx/block][yy/block+2]==false) {
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block-2]=false;
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(!Status[xx/block+2][yy/block+1] || !Status[xx/block+1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block-1][yy/block+1]) {
                        Status[xx/block+2][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block-1][yy/block]=false;
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(Status[xx/block][yy/block+3]==false) {
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block][yy/block+2]=false;
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(!Status[xx/block-2][yy/block+1] || !Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+1]) {
                        Status[xx/block-2][yy/block]=false;
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        bool=true;
                    }
                }
                break;
            
            case 1:
                if(!Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+1]) {
                    Status[xx/block][yy/block]=false;
                    Status[xx/block+1][yy/block]=false;
                    Status[xx/block][yy/block-1]=false;
                    Status[xx/block+1][yy/block-1]=false;
                    
                    colorStatus[xx/block][yy/block]=ran;
                    colorStatus[xx/block+1][yy/block]=ran;
                    colorStatus[xx/block][yy/block-1]=ran;
                    colorStatus[xx/block+1][yy/block-1]=ran;
                    bool=true;
                }
                break;
                
            case 2:
                if(pattern==0) {
                    if(!Status[xx/block][yy/block+2] || !Status[xx/block-1][yy/block+2]) {
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block-1][yy/block+1]=false;
                        
                        colorStatus[xx/block][yy/block-1]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        colorStatus[xx/block-1][yy/block+1]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+1]) {
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block-1][yy/block-1]=false;
                        
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        colorStatus[xx/block-1][yy/block-1]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(!Status[xx/block][yy/block+2] || !Status[xx/block+1][yy/block]) {
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block+1][yy/block-1]=false;
                            
                        colorStatus[xx/block][yy/block-1]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        colorStatus[xx/block+1][yy/block-1]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+2]) {
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block+1][yy/block+1]=false;
                            
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block+1]=ran;
                        bool=true;
                    }
                }
                break;
                
            case 3:
                if(pattern==0) {
                    if(!Status[xx/block][yy/block+2] || !Status[xx/block+1][yy/block+2]) {
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block+1][yy/block+1]=false;
                        
                        colorStatus[xx/block][yy/block-1]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        colorStatus[xx/block+1][yy/block+1]=ran;
                        bool=true;
                    }
                }
            
                if(pattern==1) {
                    if(!Status[xx/block+1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block-1][yy/block+2]) {
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block-1][yy/block+1]=false;
                            
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        colorStatus[xx/block-1][yy/block+1]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(!Status[xx/block-1][yy/block] || !Status[xx/block][yy/block+2]) {
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block-1][yy/block-1]=false;
                            
                        colorStatus[xx/block][yy/block-1]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        colorStatus[xx/block-1][yy/block-1]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+1]) {
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block+1][yy/block-1]=false;
                            
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block-1]=ran;
                        bool=true;
                    }
                }
                break;
                

            case 4:
                if(pattern==0) {
                    if(!Status[xx/block][yy/block+2] || !Status[xx/block+1][yy/block+1]) {
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block+1][yy/block-1]=false;
                        
                        colorStatus[xx/block][yy/block+1]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block-1]=ran;
                        bool=true;
                    }
                }
                
            if(pattern==1) {
                if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+2] || !Status[xx/block+1][yy/block+2]) {
                    Status[xx/block-1][yy/block]=false;
                    Status[xx/block][yy/block]=false;
                    Status[xx/block][yy/block+1]=false;
                    Status[xx/block+1][yy/block+1]=false;
                        
                    colorStatus[xx/block-1][yy/block]=ran;
                    colorStatus[xx/block][yy/block]=ran;
                    colorStatus[xx/block][yy/block+1]=ran;
                    colorStatus[xx/block+1][yy/block+1]=ran;
                    bool=true;
                }
            }
            
            if(pattern==2) {
                if(!Status[xx/block-1][yy/block+2] || !Status[xx/block][yy/block+1]) {
                    Status[xx/block-1][yy/block+1]=false;
                    Status[xx/block-1][yy/block]=false;
                    Status[xx/block][yy/block]=false;
                    Status[xx/block][yy/block-1]=false;
                        
                    colorStatus[xx/block-1][yy/block+1]=ran;
                    colorStatus[xx/block-1][yy/block]=ran;
                    colorStatus[xx/block][yy/block]=ran;
                    colorStatus[xx/block][yy/block-1]=ran;
                    bool=true;
                }
            }
            
            if(pattern==3) {
                if(!Status[xx/block-1][yy/block] || !Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+1]) {
                    Status[xx/block-1][yy/block-1]=false;
                    Status[xx/block][yy/block-1]=false;
                    Status[xx/block][yy/block]=false;
                    Status[xx/block+1][yy/block]=false;
                        
                    colorStatus[xx/block-1][yy/block-1]=ran;
                    colorStatus[xx/block][yy/block-1]=ran;
                    colorStatus[xx/block][yy/block]=ran;
                    colorStatus[xx/block+1][yy/block]=ran;
                    bool=true;
                }
            }
            break;
            
            case 5:
                if(pattern==0) {
                    if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+2]) {
                        Status[xx/block-1][yy/block-1]=false;
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                        
                        colorStatus[xx/block-1][yy/block-1]=ran;
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        bool=true;
                    }
                }
                
            if(pattern==1) {
                if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block]) {
                    Status[xx/block-1][yy/block]=false;
                    Status[xx/block][yy/block]=false;
                    Status[xx/block][yy/block-1]=false;
                    Status[xx/block+1][yy/block-1]=false;
                        
                    colorStatus[xx/block-1][yy/block]=ran;
                    colorStatus[xx/block][yy/block]=ran;
                    colorStatus[xx/block][yy/block-1]=ran;
                    colorStatus[xx/block+1][yy/block-1]=ran;
                    bool=true;
                }
            }
            
            if(pattern==2) {
                if(!Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+2]) {
                    Status[xx/block][yy/block-1]=false;
                    Status[xx/block][yy/block]=false;
                    Status[xx/block+1][yy/block]=false;
                    Status[xx/block+1][yy/block+1]=false;
                        
                    colorStatus[xx/block][yy/block-1]=ran;
                    colorStatus[xx/block][yy/block]=ran;
                    colorStatus[xx/block+1][yy/block]=ran;
                    colorStatus[xx/block+1][yy/block+1]=ran;
                    bool=true;
                }
            }
            
            if(pattern==3) {
                if(!Status[xx/block-1][yy/block+2] || !Status[xx/block][yy/block+2] || !Status[xx/block+1][yy/block+1]) {
                    Status[xx/block-1][yy/block+1]=false;
                    Status[xx/block][yy/block+1]=false;
                    Status[xx/block][yy/block]=false;
                    Status[xx/block+1][yy/block]=false;
                        
                    colorStatus[xx/block-1][yy/block+1]=ran;
                    colorStatus[xx/block][yy/block+1]=ran;
                    colorStatus[xx/block][yy/block]=ran;
                    colorStatus[xx/block+1][yy/block]=ran;
                    bool=true;
                }
            }
            break;
                
            case 6:
                if(pattern==0) {
                    if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+1]) {
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block][yy/block-1]=false;
                        
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        colorStatus[xx/block][yy/block-1]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(!Status[xx/block][yy/block+2] || !Status[xx/block+1][yy/block+1]) {
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                        Status[xx/block+1][yy/block]=false;
                        
                        colorStatus[xx/block][yy/block-1]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+2] || !Status[xx/block+1][yy/block+1]) {
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block+1][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                            
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block+1][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(!Status[xx/block-1][yy/block+1] || !Status[xx/block][yy/block+2]) {
                        Status[xx/block-1][yy/block]=false;
                        Status[xx/block][yy/block-1]=false;
                        Status[xx/block][yy/block]=false;
                        Status[xx/block][yy/block+1]=false;
                            
                        colorStatus[xx/block-1][yy/block]=ran;
                        colorStatus[xx/block][yy/block-1]=ran;
                        colorStatus[xx/block][yy/block]=ran;
                        colorStatus[xx/block][yy/block+1]=ran;
                        bool=true;
                    }
                }
                break;
        }
        
        return bool;
    }
    
    public static boolean leftCheck(int xx,int yy,int width,int block,boolean Status[][],int pattern,int ran) {
        
        bool=false;
        
        switch(ran) {
            case 0:
                if(pattern==0) {
                    if(xx>=block && Status[xx/block-1][yy/block+1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block-1] && Status[xx/block-1][yy/block-2]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx>=block*2 && Status[xx/block-2][yy/block]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx>=block && Status[xx/block-1][yy/block-1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block+1] && Status[xx/block-1][yy/block+2]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx>=block*3 && Status[xx/block-3][yy/block]) {
                        bool=true;
                    }
                }
                break;
                
            case 1:
                if(xx>=block && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block-1]) {
                    bool=true;
                }
                break;
                
            case 2:
                if(pattern==0) {
                    if(xx>=block*2 && Status[xx/block-2][yy/block+1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block-1]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block-2][yy/block-1]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx>=block && Status[xx/block-1][yy/block-1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block][yy/block+1]) {
                        bool=true;
                    }
                }
                break;
                
            case 3:
                if(pattern==0) {
                    if(xx>=block && Status[xx/block-1][yy/block-1] && Status[xx/block][yy/block] && Status[xx/block][yy/block+1]) {
                        bool=true;
                    }
                }
                
            if(pattern==1) {
                if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block-2][yy/block+1]) {
                    bool=true;
                }
            }
            
            if(pattern==2) {
                if(xx>=block*2 && Status[xx/block-2][yy/block-1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block+1]) {
                    bool=true;
                }
            }
            
            if(pattern==3) {
                if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block][yy/block-1]) {
                    bool=true;
                }
            }
            break;
            
            case 4:
                if(pattern==0) {
                    if(xx>=block && Status[xx/block-1][yy/block+1] && Status[xx/block-1][yy/block] && Status[xx/block][yy/block-1]) {
                        bool=true;
                    }
                }
                
            if(pattern==1) {
                if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block-1][yy/block+1]) {
                    bool=true;
                }
            }
            
            if(pattern==2) {
                if(xx>=block*2 && Status[xx/block-1][yy/block-1] && Status[xx/block-2][yy/block] && Status[xx/block-2][yy/block+1]) {
                    bool=true;
                }
            }
            
            if(pattern==3) {
                if(xx>=block*2 && Status[xx/block-2][yy/block-1] && Status[xx/block-1][yy/block]) {
                    bool=true;
                }
            }
            break;
            
            case 5:
                if(pattern==0) {
                    if(xx>=block*2 && Status[xx/block-2][yy/block-1] && Status[xx/block-2][yy/block] && Status[xx/block-1][yy/block+1]) {
                        bool=true;
                    }
                }
                
            if(pattern==1) {
                if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block-1][yy/block-1]) {
                    bool=true;
                }
            }
            
            if(pattern==2) {
                if(xx>=block && Status[xx/block-1][yy/block-1] && Status[xx/block-1][yy/block] && Status[xx/block][yy/block+1]) {
                    bool=true;
                }
            }
            
            if(pattern==3) {
                if(xx>=block*2 && Status[xx/block-1][yy/block] && Status[xx/block-2][yy/block+1]) {
                    bool=true;
                }
            }
            break;
                
            case 6:
                if(pattern==0) {
                    if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block-1][yy/block-1]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx>=block && Status[xx/block-1][yy/block-1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block-1]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx>=block*2 && Status[xx/block-2][yy/block] && Status[xx/block-1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx>=block*2 && Status[xx/block-1][yy/block-1] && Status[xx/block-2][yy/block] && Status[xx/block-1][yy/block+1]) {
                        bool=true;
                    }
                }
                break;
            }
        
        return bool;
    }
    
    public static boolean rightCheck(int xx,int yy,int width,int block,boolean Status[][],int pattern,int ran) {
        
        bool=false;
        
        switch(ran) {
            case 0:
                if(pattern==0) {
                    if(xx<width-block && Status[xx/block+1][yy/block+1] && Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block-1] && Status[xx/block+1][yy/block-2]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx<width-block*3 && Status[xx/block+3][yy/block]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx<width-block && Status[xx/block+1][yy/block-1] && Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block+1] && Status[xx/block+1][yy/block+2]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block]) {
                        bool=true;
                    }
                }
                break;
                
            case 1:
                if(xx<width-block*2 && Status[xx/block+2][yy/block] && Status[xx/block+2][yy/block-1]) {
                    bool=true;
                }
                break;
                
            case 2:
                if(pattern==0) {
                    if(xx<width-block && Status[xx/block+1][yy/block-1] && Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx<width-block*2 && Status[xx/block][yy/block-1] && Status[xx/block+2][yy/block]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block-1] && Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block+1] && Status[xx/block+2][yy/block]) {
                        bool=true;
                    }
                }
                break;
                
            case 3:
                if(pattern==0) {
                    if(xx<width-block*2 && Status[xx/block+1][yy/block-1] && Status[xx/block+1][yy/block] && Status[xx/block+2][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block] && Status[xx/block][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx<width-block  && Status[xx/block+1][yy/block-1] && Status[xx/block+1][yy/block]&& Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block] && Status[xx/block+2][yy/block-1]) {
                        bool=true;
                    }
                }
                break;
                
            case 4:
                if(pattern==0) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block-1] && Status[xx/block+2][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx<width-block*2 && Status[xx/block+1][yy/block] && Status[xx/block+2][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx<width-block && Status[xx/block+1][yy/block-1] && Status[xx/block+1][yy/block] && Status[xx/block][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx<width-block*2 && Status[xx/block+1][yy/block-1] && Status[xx/block+2][yy/block]) {
                        bool=true;
                    }
                }
                break;
            
            case 5:
                if(pattern==0) {
                    if(xx<width-block && Status[xx/block][yy/block-1] && Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block-1] && Status[xx/block+1][yy/block]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx<width-block*2 && Status[xx/block+1][yy/block-1] && Status[xx/block+2][yy/block] && Status[xx/block+2][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                break;
                
            case 6:
                if(pattern==0) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block] && Status[xx/block+1][yy/block-1]) {
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    if(xx<width-block*2 && Status[xx/block+1][yy/block-1] && Status[xx/block+2][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==2) {
                    if(xx<width-block*2 && Status[xx/block+2][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    if(xx<width-block && Status[xx/block+1][yy/block-1] && Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
        }
        
        return bool;
    }
    
    public static boolean rightTurnCheck(int xx,int yy,int width,int block,boolean Status[][],int pattern,int ran) {
        
        bool=false;
        
        switch(ran) {
            
            case 0:
                if(pattern==0) {
                    if(xx>=block && xx<width-block*2) {
                        for(int i=1;i<3;i++) {
                            for(int j=0;j<3;j++) {
                                if(!Status[xx/block+i][yy/block-j]) {
                                    return bool; 
                                }
                            }
                        }
                        if(!Status[xx/block-1][yy/block] || !Status[xx/block-1][yy/block-1]) {
                            return bool;
                        }
                        
                        bool=true;
                    }
                }
                
                if(pattern==1) {
                    for(int i=0;i<3;i++) {
                        for(int j=1;j<3;j++) {
                            if(!Status[xx/block+i][yy/block+j]) {
                                return bool;
                            }
                        }
                    }
                    if(!Status[xx/block][yy/block-1] || !Status[xx/block-1][yy/block-1]) {
                        return bool;
                    }
                    
                    bool=true;
                }
                
                if(pattern==2) {
                    if(xx>=block*2 && xx<width-block) {
                        for(int i=1;i<3;i++) {
                            for(int j=0;j<3;j++) {
                                if(!Status[xx/block-i][yy/block+j]) {
                                    return bool; 
                                }
                            }
                        }
                        if(!Status[xx/block+1][yy/block] || !Status[xx/block+1][yy/block+1]) {
                            return bool;
                        }
                        
                        bool=true;
                    }
                }
                
                if(pattern==3) {
                    for(int i=0;i<3;i++) {
                        for(int j=1;j<3;j++) {
                            if(!Status[xx/block-i][yy/block+j]) {
                                return bool;
                            }
                        }
                    }
                    if(!Status[xx/block][yy/block+1] || !Status[xx/block+1][yy/block+1]) {
                        return bool;
                    }
                    
                    bool=true;
                }
                
                break;
                
            case 1:
                
                bool=true;
                break;
                
            case 2:
                
                if(pattern==0) {
                    if(xx<width-block) {
                        for(int i=0;i<2;i++) {
                            if(!Status[xx/block+1][yy/block-i] || !Status[xx/block-1][yy/block-i]){
                                return bool;
                            }
                        }
                        bool=true;
                    }
                }

                if(pattern==1) {
                    for(int i=0;i<2;i++) {
                        if(!Status[xx/block+i][yy/block+1] || !Status[xx/block+i][yy/block-1]) {
                            return bool;
                        }
                    }
                    bool=true;
                }

                if(pattern==2) {
                    if(xx>=block) {
                        for(int i=0;i<2;i++) {
                            if(!Status[xx/block+1][yy/block+i] || !Status[xx/block-1][yy/block+i]){
                                return bool;
                            }
                        }
                        bool=true;
                    }
                }

                if(pattern==3) {
                    for(int i=0;i<2;i++) {
                        if(!Status[xx/block-i][yy/block+1] || !Status[xx/block-i][yy/block-1]) {
                            return bool;
                        }
                    }
                    bool=true;
                }
                
                break;
                
            case 3:
                
                if(pattern==0 ||pattern==2) {
                    if(xx>=block && xx<width-block) {
                        for(int i=0;i<2;i++) {
                            if(!Status[xx/block+1][yy/block-i] || !Status[xx/block-1][yy/block+i]){
                                return bool;
                            }
                        }
                        bool=true;
                    }
                }

                if(pattern==1 || pattern==3) {
                    for(int i=0;i<2;i++) {
                        if(!Status[xx/block+i][yy/block+1] || !Status[xx/block-i][yy/block-1]) {
                            return bool;
                        }
                    }
                    bool=true;
                }
                
                break;
                
            case 4:

                if(pattern==0) {
                    if(xx>=block) {
                        if(Status[xx/block+1][yy/block+1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block+1]) {
                            bool=true;
                        }
                    }
                }

                if(pattern==1) {
                    if(Status[xx/block][yy/block-1] && Status[xx/block-1][yy/block-1] && Status[xx/block-1][yy/block+1]) {
                        bool=true;
                    }
                }

                if(pattern==2) {
                    if(xx<width-block) {
                        if(Status[xx/block+1][yy/block+1] && Status[xx/block+1][yy/block] && Status[xx/block-1][yy/block-1]) {
                            bool=true;
                        }
                    }
                }

                if(pattern==3) {
                    if(Status[xx/block][yy/block+1] && Status[xx/block+1][yy/block+1] && Status[xx/block+1][yy/block-1]) {
                        bool=true;
                    }
                }
                break;
                
            case 5:

                if(pattern==0) {
                    if(xx<width-block) {
                        if(Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block-1] && Status[xx/block][yy/block-1] && Status[xx/block-1][yy/block+1]) {
                            bool=true;
                        }
                    }
                }

                if(pattern==1) {
                    if(Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block+1] && Status[xx/block][yy/block+1] && Status[xx/block-1][yy/block-1]) {
                        bool=true;
                    }
                }

                if(pattern==2) {
                    if(xx>=block) {
                        if(Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block+1] && Status[xx/block][yy/block+1] && Status[xx/block+1][yy/block-1]) {
                            bool=true;
                        }
                    }
                }

                if(pattern==3) {
                    if(Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block-1] && Status[xx/block][yy/block-1] && Status[xx/block+1][yy/block+1]) {
                        bool=true;
                    }
                }
                break;
                
            case 6:

                if(pattern==0) {
                    if(Status[xx/block+1][yy/block+1] && Status[xx/block+1][yy/block-1] && Status[xx/block][yy/block+1] && Status[xx/block-1][yy/block-1]) {
                        bool=true;
                    }
                }

                if(pattern==1) {
                    if(xx>=block) {
                        if(Status[xx/block+1][yy/block+1] && Status[xx/block+1][yy/block-1] && Status[xx/block-1][yy/block] && Status[xx/block-1][yy/block+1]) {
                            bool=true;
                        }
                    }
                }

                if(pattern==2) {
                    if(Status[xx/block+1][yy/block+1] && Status[xx/block][yy/block-1] && Status[xx/block-1][yy/block+1] && Status[xx/block-1][yy/block-1]) {
                        bool=true;
                    }
                }

                if(pattern==3) {
                    if(xx<width-block) {
                        if(Status[xx/block+1][yy/block] && Status[xx/block+1][yy/block-1] && Status[xx/block-1][yy/block+1] && Status[xx/block-1][yy/block-1]) {
                            bool=true;
                        }
                    }
                }
                
                break;
        }
        
        return bool;
    }
}
	
